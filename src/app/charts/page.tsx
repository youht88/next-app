"use client";

import { Line } from '@ant-design/charts';
import { HomeOutlined } from '@ant-design/icons';
import { Button, Space } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Fragment, useState, } from 'react';
function Page(){
    const router = useRouter();
    const initData = [
    { year: '1991', value: 3 },
    { year: '1992', value: 4 },
    { year: '1993', value: 3.5 },
    { year: '1994', value: 5 },
    { year: '1995', value: 4.9 },
    { year: '1996', value: 6 },
    { year: '1997', value: 7 },
    { year: '1998', value: 9 },
    { year: '1999', value: 13 },
    ];
    const [data,setData] = useState(initData);
    function handleAdd(){
        setData(
            [...data,{year:'2000',value:15}]
        );
    }
    function handleDelete(){
        setData(
            data.filter((item)=>item.year!='2000')
        );
    }
    function handleUpdate(){
        setData(
            data.map((item)=>{
                if (item.year=='2000'){
                    return {"year":'2000',value:8};
                }else{
                    return item;
                }
            })
        );
    }
 
    const config = {
        data,
        height: 400,
        xField: 'year',
        yField: 'value',
        point: {
            size: 5,
            shape: 'diamond',
        },
    };
    const handleClick = ()=>{
        router.push('/pro')
    }
    return (
        <Fragment key="line">
        <Link href="/pro">pro</Link>
        <Button onClick={handleClick}>go</Button>
        <Line {...config} />
        <Space  style={{width:150,padding:8,overflow:"scroll",fontSize:16,color:"red",borderStyle:"dashed",borderColor:"red",borderWidth:1,borderTopLeftRadius:15,
        backgroundColor:"yellow",
        backgroundImage:"url(https://img.alicdn.com/tfs/TB1YHEpwUT1gK0jSZFhXXaAtVXa-28-27.svg)"
        }}>
            
            <Button onClick={handleAdd} icon={<HomeOutlined/>}> add </Button>
            <Button onClick={handleUpdate} style={{color:"green"}}> update </Button>
            <Button onClick={handleDelete}> delete </Button>
            <div style={{position:"absolute",top:10,left:50}}>test <b><i style={{color:"blue"}}>Line</i></b> of antd-charts</div>
        </Space>
        </Fragment>
    );
}
export default Page;