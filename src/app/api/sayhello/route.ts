import { NextApiRequest } from "next"

export function GET(
   req: NextApiRequest,
 ) {
   return Response.json({ req:req.url,message: 'Hello from Next.js!' })
 }