"use client";

import { GithubFilled, HomeOutlined, InfoCircleFilled } from '@ant-design/icons';
import { PageContainer, ProCard, ProLayout, ProSettings } from '@ant-design/pro-components';
import { useState } from 'react';
import defaultprops from './_defaultprops';

export default function Page(){
  const [settings, setSetting] = useState<Partial<ProSettings> | undefined>({
    layout: 'side',
  });
  const [pathname, setPathname] = useState('/list/sub-page/sub-sub-page1');
  
  const actionsRender = (props:any)=>{
    return [<HomeOutlined key='home'/>,<GithubFilled key='github'/>,<InfoCircleFilled key='info'/>]
  }
  
  const menuFooterRender = (props:any)=>(<div>ylzinfo.com</div>)
  return (
  <ProLayout title="ProLayout Test" 
    {...defaultprops}
    location= {{pathname}}
    menu={{
      type: 'group',
    }}
    actionsRender={actionsRender}
    avatarProps = {{
      src: 'https://gw.alipayobjects.com/zos/antfincdn/efFD%24IOql2/weixintupian_20170331104822.jpg',
      size: 'small',
      title: '龙猫',
    }}
    onMenuHeaderClick={(e) => console.log(e)}
    menuFooterRender={menuFooterRender}
    {...settings}
  >
    <PageContainer>
          <ProCard
            style={{
              height: '100%'
              //height: '100vh',
              //minHeight: 800,
            }}
          >
            <div />
          </ProCard>
        </PageContainer>
  </ProLayout>
  )
}