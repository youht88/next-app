// export default function Test(){
//     const name: string = "youht"
//     let msg:string = ""
//     function handleTest(): void{
//         msg =  "hello "+name 
//     }
//     return (
//         <h1 onClick={handleTest}> {msg}</h1>
//     )
// }
"use client";

import { Button, Divider, Row } from 'antd';
import { useRouter } from 'next/navigation';
import { useState } from 'react';
import Draggable from 'react-draggable';
interface Info{
    name: string;
}

function Hello(_props:any){
    return (
        <ul> 
            {_props.infos.map(
                (info:Info)=>{
                    return (<li key={info.name}>
                        <Row align='middle' justify='start' style={{padding:8}}>
                                <h1> {`hello ${info.name}`},score:{_props.score}</h1>
                                <div style={{padding:8}}></div>
                                <Button type="primary" onClick={_props.onClick}>Change</Button>
                        </Row>
                      </li>)
                }
            )}            
        </ul>
    )
}
function _Div(props:any){
    return (
        <div style={{width:50,height:50,margin:8,backgroundColor:props.color}}></div>
    )
}
function App(){
    const [score,setScore] = useState(0)
    const infos = [{"name":"youht"},{"name":"jinli"},{"name":"youyc"}];
    const router = useRouter()
    function gotoTest(){
        router.push('/pro')
    }
    
    function handleClick(){
       setScore(score+1)
    }
    return (
        <>
        <Row>
            <Draggable>
                <div>
                    <_Div color="red"></_Div>
                </div>
            </Draggable>
            <_Div color="green"></_Div>
        </Row>
        
        <Divider dashed={true}/>
        <Button type="link" onClick={gotoTest}> hello </Button>
        
        <Hello infos={infos} score={score} onClick={()=>handleClick()} />
        </>
    )
}

export default App